class myclass {
file {'/root/devops.txt':                                         # resource type file and filename
      ensure  => present,                                               # make sure it exists
      mode    => '0644',                                                # file permissions
      content => template('myclass/test.cfg.erb'),                      # note the ipaddress_eth0 fact
}    

exec { 'installing jenkins repo file':
command => 'wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo',
path    => '/usr/local/bin/:/bin/',
}

exec { 'installing jenkins key':
command => 'rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key',
path    => '/usr/local/bin/:/bin/',
}

$admintools = ['jenkins','java','unzip','mlocate','net-tools','elinks','git','wget','httpd','vsftpd','docker']

package { $admintools:
  ensure => installed,
}

$servicestart  = ['httpd','vsftpd','docker','jenkins']

service { $servicestart:
  ensure => running,
  enable => true,
}

file {'index.html':                                            # resource type file and filename
      ensure  => present,                                               # make sure it exists
      mode    => '0644',                                                # file permissions
      path    => '/var/www/html/index.html',
      content => template('myclass/index.html.erb'),                          # note the ipaddress_eth0 fact
    }

}


